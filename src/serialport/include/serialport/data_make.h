#ifndef  _DATA_MAKE
#define  _DATA_MAKE

#include <ros/ros.h> 
#include <serial/serial.h>  //ROS已经内置了的串口包 
#include <std_msgs/String.h> 
#include <std_msgs/Empty.h> 
#include "serialport/ALL_UAVs_LLA.h"
#include "geographic_msgs/GeoPoint.h"
#include <string>
#include <time.h>
#include <sstream>

//函数的定义
// 将64位数据——》8位数据
uint8_t split(double data, int position);
// 将4个8位数据——》64位数据
double merge(uint8_t first, uint8_t second, uint8_t third, uint8_t fourth);
// 异或校验
uint8_t data_crc(uint8_t *p_data, int len_data);

#endif