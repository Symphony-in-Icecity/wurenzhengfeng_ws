#include "serialport/data_make.h"

uint8_t split(double data, int position)
{
    uint8_t result = 0;
    uint32_t data_32 = (uint32_t)(data*1e7);
    switch (position)
    {
        case 1:{result = (uint8_t)(data_32 >> 24);return result;}
        break;
        case 2:{result = (uint8_t)((data_32 & 0x00ff0000) >> 16);return result;}
        break;
        case 3:{result = (uint8_t)((data_32 & 0x0000ff00) >> 8);return result;}
        break;
        case 4:{result = (uint8_t)(data_32 & 0x000000ff);return result;}
        break;
    }
}

double merge(uint8_t first, uint8_t second, uint8_t third, uint8_t fourth)
{
    uint32_t temp = 0;
    double result = 0;
    temp = ((uint32_t)first<<24);
    temp |= ((uint32_t)second<<16);
	temp |= ((uint32_t)third<<8);
	temp |= ((uint32_t)fourth); 
    result = (double)(temp / 10000000.0);
}

uint8_t data_crc(uint8_t *p_data, int len_data)
{
    uint8_t result = 0;
    while (len_data--)
    {
        result = result ^ (*p_data++);
    }
    return result;
}