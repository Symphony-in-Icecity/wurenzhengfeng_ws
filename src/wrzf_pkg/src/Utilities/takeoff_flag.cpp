#include <ros/ros.h>
#include <iostream>
#include <std_msgs/Bool.h>
#include <mavros_msgs/RCIn.h>
using namespace std;


mavros_msgs::RCIn RCIn_data; 
ros::Publisher  takeoff_pub;
ros::Subscriber RCIn_sub;

void RCIn_sub_cb(const mavros_msgs::RCIn::ConstPtr& msg)
{
    std_msgs::Bool takeoff_order;
    RCIn_data = *msg;
    if (RCIn_data.channels[6]>1700)//1494,1944
    {
        takeoff_order.data = true;
    }
    takeoff_pub.publish(takeoff_order);
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "takeoff_flag");
    ros::NodeHandle nh;
    //rostopic pub /px4_command/takeoff_flag std_msgs/Bool "data: true" 
    RCIn_sub = nh.subscribe<mavros_msgs::RCIn>("/mavros/rc/in", 10, RCIn_sub_cb);
    takeoff_pub = nh.advertise<std_msgs::Bool>("/px4_command/takeoff_flag",10);
    ros::spin();//循环读取接收的数据
}

