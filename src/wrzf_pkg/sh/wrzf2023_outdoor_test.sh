##无人争锋比赛机室外测试飞行脚本
gnome-terminal --window -e 'bash -c "roslaunch mavros px4.launch fcu_url:="/dev/ttyTHS0:921600"; exec bash"' \
--tab -e 'bash -c "sleep 5; roslaunch usb_cam usb_cam-test.launch; exec bash"' \
--tab -e 'bash -c "sleep 5; roslaunch yolov5_tensorrtx yolov5.launch; exec bash"' \
--tab -e 'bash -c "sleep 5; rosrun TFLuna_i2c TFLuna_avoid.py; exec bash"' \
--tab -e 'bash -c "sleep 10; roslaunch wrzf_pkg px4_pos_estimator.launch; exec bash"' \
--tab -e 'bash -c "sleep 10; roslaunch wrzf_pkg position_controller.launch; exec bash"' \
--tab -e 'bash -c "sleep 10; rosrun wrzf_pkg move; exec bash"' \
--tab -e 'bash -c "sleep 10; rosrun wrzf_pkg set_mode; exec bash"' \
