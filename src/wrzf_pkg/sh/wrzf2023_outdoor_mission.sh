##无人争锋比赛机室外自主任务飞行脚本
#source /home/orin/.bashrc
gnome-terminal --window -e 'bash -c "roslaunch mavros px4.launch fcu_url:="/dev/ttyTHS0:921600"; exec bash"' \
--tab -e 'bash -c "sleep 5; roslaunch usb_cam usb_cam-test.launch; exec bash"' \
--tab -e 'bash -c "sleep 5; roslaunch yolov5_tensorrtx yolov5.launch; exec bash"' \
--tab -e 'bash -c "sleep 5; rosrun TFLuna_i2c TFLuna_avoid.py; exec bash"' \
--tab -e 'bash -c "sleep 10; roslaunch wrzf_pkg px4_pos_estimator.launch; exec bash"' \
--tab -e 'bash -c "sleep 10; roslaunch wrzf_pkg mission_planner.launch; exec bash"' \
--tab -e 'bash -c "sleep 10; rosrun wrzf_pkg takeoff_flag; exec bash"' \
#--tab -e 'bash -c "sleep 10; rosbag record --size=15360 --duration=900 -e "/mavros.*" -e "/vins_estimator.*" /odom_with_gps ; exec bash"' \
#--tab -e 'bash -c "sleep 15; ./wrzf_cam_vio.sh; exec bash"' \
#--tab -e 'bash -c "sleep 5; rosrun pic_cap_pkg pic_cap; exec bash"' \
